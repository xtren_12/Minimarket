var client = require("../database/db");
var db = client.db("certus");// SELECCIONANDO LA BASE DE DATOS

var controller = {
    listar: function (req, res) {
        console.log("-------------------");
        console.log("Entrando a la funcion deco ");
        db.collection("deco").find().toArray()
            .then(
                deco => {
                    res.render('deco_list', { dataDeco: deco });
                }
            ).catch(
                error => console.log(error)
            )
    },
    form: function (req, res) {
        console.log("-------------------");
        console.log("entrando a la funcion form");
        console.log("id:" + req.params.id);
        if (req.params.id == 1) {
            var deco = {}
            deco.nombre = "";
            deco.Gama = "";
            deco.precio = "";
            res.render('deco_form', { decoForm: deco });
        }


    },
    save: function (req, res){
        console.log("-------------------");
        console.log("ENTRADA A LA FUNCION GUARDAR");
        console.log(req.body);
        if(req.body.nombre){
            var deco = {}
            deco.nombre= req.body.nombre;
            deco.Gama = req.body.Gama;
            deco.precio = req.body.precio;
            console.log(deco);
            db.collection("deco").insertOne(deco).then(
                ()=>{
                    res.redirect('/views/deco/list');
                }
            ).catch(
                error => console.log(error)
            )
        }
    }
}
module.exports = controller;