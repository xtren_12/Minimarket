var client = require("../database/db");
var db = client.db("certus");// SELECCIONANDO LA BASE DE DATOS

var controller = {
    listar: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION LISTAR");
        db.collection("joyeria").find().toArray()
            .then(
                joyeria => {
                    res.render('joyeria_list', { dataJoyeria: joyeria });
                }
            ).catch(
                error => console.log(error)
            )
        },
        form: function (req, res) {
            console.log("-------------------");
            console.log("ENTRANDO A LA FUNCION FORM");
            console.log("id:" + req.params.id);
            if (req.params.id == 1) {
                var joyeria = {}
                joyeria.nombre = "";
                joyeria.Gama = "";
                joyeria.precio = "";
                res.render('deco_form', { joyeriaForm: joyeria });
            }
    
    
        },
        save: function (req, res){
            console.log("-------------------");
            console.log("ENTRADA A LA FUNCION GUARDAR");
            console.log(req.body);
            if(req.body.nombre){
                var joyeria = {}
                joyeria.nombre= req.body.nombre;
                joyeria.Gama = req.body.Gama;
                joyeria.precio = req.body.precio;
                console.log(joyeria);
                db.collection("joyeria").insertOne(joyeria).then(
                    ()=>{
                        res.redirect('/views/joyeria/list');
                    }
                ).catch(
                    error => console.log(error)
                )
            }
        }
    }
    module.exports = controller;