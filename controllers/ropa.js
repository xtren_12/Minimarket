var client = require("../database/db");
var db = client.db("certus");// SELECCIONANDO LA BASE DE DATOS

var controller = {
    listar: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION LIST");
        db.collection("ropa").find().toArray()
            .then(
                ropa => {
                    res.render('ropa_list', { dataRopa: ropa });
                }
            ).catch(
                error => console.log(error)
            )
    },
    
    form: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION FORM");
        console.log("id:" + req.params.id);
        if (req.params.id == 1) {
            var ropa = {}
            ropa.nombre = "";
            ropa.categoria = "";
            ropa.marca = "";
            ropa.precio = "";
            res.render('ropa_form', { ropaForm:ropa });
        }


    },

 save: function (req, res){
    console.log("-------------------");
    console.log("ENTRADA A LA FUNCION GUARDAR");
    console.log(req.body);
    if(req.body.nombre){
        var ropa = {}
        ropa.nombre = req.body.nombre;
        ropa.IDPRODUCTO = req.body.IDPRODUCTO;
        ropa.PRECIO = req.body.PRECIO;
        console.log(ropa);
        db.collection("ropa").insertOne(ropa).then(

            ()=>{
                res.redirect('/views/ropa/list');
            }
        ).catch(
            error => console.log(error)
        )
    }
}




}
module.exports = controller;