var client = require("../database/db");
var db = client.db("certus");// SELECCIONANDO LA BASE DE DATOS

var controller = {
    listar: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION LISTAR ");
        db.collection("electro").find().toArray()
            .then(
                electro => {
                    res.render('electro_list', { dataElectro: electro });
                }
            ).catch(
                error => console.log(error)
            )
    },
    form: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION FORM");
        console.log("id:" + req.params.id);
        if (req.params.id == 1) {
            var electro = {}
            electro.nombre = "";
            electro.PRECIO = "";
            res.render('electro_form', { electroForm: electro });
        }


    },
    save: function (req, res){
        console.log("-------------------");
        console.log("ENTRADA A LA FUNCION GUARDAR");
        console.log(req.body);
        if(req.body.nombre){
            var electro = {}
            electro.nombre= req.body.nombre;
            electro.PRECIO = req.body.PRECIO;
            console.log(electro);
            db.collection("electro").insertOne(electro).then(
                ()=>{
                    res.redirect('/views/electro/list');
                }
            ).catch(
                error => console.log(error)
            )
        }
    }
}
module.exports = controller;
