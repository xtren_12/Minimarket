var express = require('express');
var RopaController = require('../controllers/ropa');


var router = express.Router();

// RUTAS PARA PRODUCTO
router.get('/views/ropa/list', RopaController.listar);
router.get('/views/ropa/form/:id', RopaController.form);//abrir formularia
router.post('/ropa/save',RopaController.save);
module.exports = router;