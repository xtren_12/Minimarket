var express = require('express');
var ElectroController = require('../controllers/electro');


var router = express.Router();

// RUTAS PARA PRODUCTO
router.get('/views/electro/list', ElectroController.listar);
router.get('/views/electro/:id', ElectroController.form);
router.post('/electro/save', ElectroController.save);


module.exports = router;
