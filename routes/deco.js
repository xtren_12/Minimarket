var express = require('express');
var DecoController = require('../controllers/deco');


var router = express.Router();

// RUTAS PARA PRODUCTO
router.get('/views/deco/list', DecoController.listar);
router.get('/views/deco/:id', DecoController.form);
router.post('/deco/save',DecoController.save);

module.exports = router;