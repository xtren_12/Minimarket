var express = require('express');
var JoyeriaController = require('../controllers/joyeria');


var router = express.Router();

// RUTAS PARA PRODUCTO
router.get('/views/joyeria/list', JoyeriaController.listar);
router.get('/views/joyeria/:id', JoyeriaController.form);
router.post('/joyeria/save', JoyeriaController.save);

module.exports = router;