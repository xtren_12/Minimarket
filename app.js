'use strict'

// REQUIRES
var express = require('express');

// CARGAR ARCHIVOS DE RUTAS
var joyeria_routes = require('./routes/joyeria');
var ropa_routes = require('./routes/ropa');
var electro_routes = require('./routes/electro');
var deco_routes = require('./routes/deco');
var general_routes = require('./routes/general');


// Ejecutar Express
var app = express();

// Asigno Ejs a las vistas
app.set('view engine','ejs');

// Decodificacion de envios Form
app.use(express.urlencoded({extended:false}));
app.use(express.json());




// Reescribir rutas
app.use('/',joyeria_routes);
app.use('/',ropa_routes);
app.use('/',electro_routes);
app.use('/',deco_routes);
app.use('/',general_routes);


//Exportar module
module.exports = app;